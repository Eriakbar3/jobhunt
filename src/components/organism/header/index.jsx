import React from 'react';
import './index.css';
import logo from '../../../assets/image/logo.png';
import { Button } from 'antd';
export default function Header() {
  return (
    <div className="header">
      <div className="header-left">
        <div className="logo">
          <img src={logo} />
        </div>
        <div>
          <ul className="menu-list">
            <li className="list-active">Find Jobs</li>
            <li>Browse Companies</li>
          </ul>
        </div>
      </div>
      <div className="header-right">
        <Button className="btn-login" size="middle">
          Login
        </Button>
        <div style={{ width: 1, background: '#D6DDEB' }}></div>
        <Button type="primary">Register</Button>
      </div>
    </div>
  );
}
