import React, { useEffect } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetBlog, actionShowBlog } from '../../redux/blog/action';
import LabelCustom from '../../components/atoms/Label';
export default function Blog() {
  const data = {
    id: 123,
  };
  const dispatch = useDispatch();
  const { blog, loading, blogDetail } = useSelector((state) => state.blog);
  useEffect(() => {
    getDataBlog();
  }, []);
  const getDataBlog = async () => {
    dispatch(actionGetBlog());
  };
  const showDataBlog = (id) => {
    dispatch(actionShowBlog(id));
  };
  return (
    <div>
      <LabelCustom text="123" />
      {loading && 'Loading'}
      {blog.map((data, index) => {
        return (
          <>
            <button onClick={() => showDataBlog(index)}>{data.title}</button>
            <p>{data.description}</p>
          </>
        );
      })}
      <hr />
      {blogDetail && (
        <>
          <h1>{blogDetail.title}</h1>
          <p>{blogDetail.description}</p>
        </>
      )}
    </div>
  );
}
