import React from 'react';
import HeaderTemplate from '../../../components/organism/header';
import SearchContent from './SearchContent';
import MainContent from './MainContent';
export default function FindJob() {
  return (
    <div>
      <HeaderTemplate />
      <SearchContent />
      <MainContent />
    </div>
  );
}
