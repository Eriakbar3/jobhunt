import React, { useEffect } from 'react';
import Collapse from '../../../../../components/atoms/Collapse';
import { Checkbox } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { actionGetFilter } from '../../../../../redux/job/action';
import './index.css';
export default function FilterCategory() {
  const dispatch = useDispatch();
  const { jobType, jobCategory, jobLevel, jobSalary } = useSelector((state) => state.job);
  useEffect(() => {
    dispatch(actionGetFilter());
    console.log(jobType);
  }, []);
  const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;
  const items = [
    {
      key: '1',
      label: 'Type of Employment',
      children: (
        <div className="child-select">
          {jobType.map((data) => {
            return <Checkbox>{data.category}</Checkbox>;
          })}
        </div>
      ),
    },
    {
      key: '2',
      label: 'Categories',
      children: (
        <div className="child-select">
          {jobCategory.map((data) => {
            return <Checkbox>{data.category}</Checkbox>;
          })}
        </div>
      ),
    },
    {
      key: '3',
      label: 'Job Level',
      children: (
        <div className="child-select">
          {jobLevel.map((data) => {
            return <Checkbox>{data.category}</Checkbox>;
          })}
        </div>
      ),
    },
    {
      key: '4',
      label: 'Salary Range',
      children: (
        <div className="child-select">
          {jobSalary.map((data) => {
            return (
              <Checkbox>
                ${data.from} - ${data.to}
              </Checkbox>
            );
          })}
        </div>
      ),
    },
  ];
  return <Collapse items={items} defaultKey={['1', '2', '3', '4']} />;
}
