import { Breadcrumb, Layout, Menu, theme } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
import { useSelector } from 'react-redux';
const App = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { counter, loading } = useSelector((state) => state.counter);
  const { umur } = useSelector((state) => state.user);
  return (
    <>
      <p>{umur}</p>
    </>
  );
};
export default App;
