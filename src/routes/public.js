
import Blog from "../pages/blog";
import Home from "../pages/home";
import FindJob from "../pages/job/find-job";
import Login from "../pages/login";

const publicRoute = [
  {
    key: "login",
    path: "/login",
    element: <Login />,
  },
  {
    key: "Home",
    path: "/",
    element: <Home />,
  },
  {
    key: "Blog",
    path: "/blog",
    element: <Blog />,
  },
  {
    key: "FindJob",
    path: "/find-job",
    element: <FindJob />,
  }
];

export default publicRoute;
