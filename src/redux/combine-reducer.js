import { combineReducers } from 'redux';
import {  persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import counter from './counter/reducer'
import user from './user/reducer'
import blog from './blog/reducer'
import job from './job/reducer'
const persistConfig = {
  key: 'root',
  storage,
};
const combineReducer = combineReducers({
  counter,
  user,
  blog,
  job
});
const persistedReducer = persistReducer(persistConfig, combineReducer);
export default persistedReducer;