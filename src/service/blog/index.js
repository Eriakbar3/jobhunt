import axios from 'axios'
import { blogPath,showBlogPath } from '../path'
export const serviceGetBlog = async ()=> {
    return await axios.post(blogPath)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}

export const serviceShowBlog = async (data)=> {
    return await axios.post(showBlogPath,data)
        .then((res) => Promise.resolve(res))
        .catch((err) => Promise.reject(err));
}
