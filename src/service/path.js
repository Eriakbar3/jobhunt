const baseUrl = 'http://localhost:3004'

export const blogPath = `${baseUrl}/blog`;
export const deleteBlogPath = `${baseUrl}/blog/delete`;
export const updateBlogPath = `${baseUrl}/blog/update`;
export const showBlogPath = `${baseUrl}/blog/show`;
export const jobPath = `${baseUrl}/api/job`;
export const jobTypePath = `${baseUrl}/api/job/type`;
export const jobCategoryPath = `${baseUrl}/api/job/category`;
export const jobLevelPath = `${baseUrl}/api/job/level`;
export const jobSalaryPath = `${baseUrl}/api/job/range-salary`;